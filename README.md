# Fluorine 💨

---

**(🚧 Warning: Currently experimental 🚧)**

---

**Fluroine** is a composable reactive web framework that doesn't get in the way, based on Haskell's **[Shpadoinkle](https://shpadoinkle.org/)**:

```haskell
main :: Effect Unit
main = launchAff_ $ fluorine (==) identity 0 body

body :: forall m. View Html m Int
body c = h "div" []
  [ h "p" [] [t $ show c]
  , h "button" [on "click" (_ - 1)] [t "-"]
  , h "button" [on "click" (_ + 1)] [t "+"]
  ]
```
