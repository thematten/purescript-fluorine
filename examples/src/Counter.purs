module Counter where

import Prelude

import Effect (Effect)
import Effect.Aff (launchAff_)
import Fluorine (Html, View, fluorine, h, t, on)

main :: Effect Unit
main = launchAff_ $ fluorine (==) identity 0 body

body :: forall m. View Html m Int
body c = h "div" []
  [ h "p" [] [t $ show c]
  , h "button" [on "click" (_ - 1)] [t "-"]
  , h "button" [on "click" (_ + 1)] [t "+"]
  ]
