module TextTransform where

import Prelude

import Effect (Effect)
import Effect.Aff (launchAff_)
import Effect.Class (class MonadEffect)
import Fluorine (Html, View, fluorine, h, t, onInput, (:=), onRaw)
import Fluorine.Continuation as C
import Data.Maybe (Maybe (..), fromJust)
import Partial.Unsafe (unsafePartial)
import Data.String as Str
import Web.Event.Event (target) as D
import Web.HTML.HTMLSelectElement as Select

type Model = { txt :: String, fun :: String -> String }

main :: Effect Unit
main = launchAff_ $
  fluorine (\_ _ -> false) identity { txt: "", fun: identity } body

body :: forall m. MonadEffect m => View Html m Model
body m = h "div" []
  [ C.liftR _{txt=_} _.txt
    $ h "textarea"
    [ "autofocus" := "true"
    , "style"     := "width: 100%"
    , onInput const
    ] []
  , h "select"
    [ onRaw "change" \e -> Just do
        val <- Select.value $ unsafePartial $ fromJust $
          Select.fromEventTarget =<< D.target e
        pure $ C.pure _{ fun = nameToFun val }
    ]
    $ option <$> ["none", "lower", "upper"]
  , h "textarea"
    [ "disabled" := "true"
    , "style"    := "width: 100%"
    ] [t $ m.fun m.txt]
  ]
 where
  option name = h "option" ["value" := name] [t name]
  nameToFun = case _ of
    "lower" -> Str.toLower
    "upper" -> Str.toUpper
    _       -> identity
