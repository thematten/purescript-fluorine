module Async where

import Prelude

import Data.Newtype (wrap)
import Effect (Effect)
import Effect.Aff (Aff, launchAff_, delay)
import Fluorine (Html, View, fluorine, h, t, on, onM)

main :: Effect Unit
main = launchAff_ $ fluorine (==) identity 0 body

body :: View Html Aff Int
body c = h "div" []
  [ h "button" [on "click" \c' -> case expensive' unit of unit -> c' + 1] [t "Slow"]
  , h "button" [onM "click" $ delay (wrap 3000.0) $> (_ + 1)] [t "Fast"]
  , h "button" [on "click" (_ + 1)] [t "Normal"]
  , h "p" [] [t $ show c]
  ]

-- Emulates synchronous delay
expensive' :: Unit -> Unit
expensive' _ = go 500000000 where
  go 0 = unit
  go x = go (x - 1)
