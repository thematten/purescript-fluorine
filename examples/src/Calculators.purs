module Calculators where

import Prelude

import Data.Int (fromString)
import Data.Maybe (Maybe (..), fromMaybe)
import Effect (Effect)
import Effect.Aff (launchAff_)
import Fluorine (Html, View, fluorine, h, t, (:=), on, onInput)
import Fluorine.Continuation as C

main :: Effect Unit
main = launchAff_ $ fluorine (==) identity { _1, _2, _3 } views where
  _1@_2@_3 = { operation: Addition, left: 0, right: 0 }

views
  :: forall m
   . Functor m
  => View Html m { _1 :: Model, _2 :: Model, _3 :: Model }
views r = h "div" []
  [ C.liftRV _{ _1 = _ } _._1 view r
  , C.liftRV _{ _2 = _ } _._2 view r
  , C.liftRV _{ _3 = _ } _._3 view r
  ]

type Model =
  { operation :: Operation
  , left      :: Int
  , right     :: Int
  }

data Operation
  = Addition
  | Subtraction
  | Multiplication
  | Division

derive instance eqOperation :: Eq Operation

opFunction :: Operation -> (Int -> Int -> Int)
opFunction = case _ of
  Addition       -> (+)
  Subtraction    -> (-)
  Multiplication -> (*)
  Division       -> \x y ->
    if y == 0 then 0 else div x y


opText :: Operation -> String
opText = case _ of
  Addition       -> "+"
  Subtraction    -> "-"
  Multiplication -> "×"
  Division       -> "÷"

readOp :: String -> Maybe Operation
readOp = case _ of
  "+" -> Just Addition
  "-" -> Just Subtraction
  "×" -> Just Multiplication
  "÷" -> Just Division
  _   -> Nothing

opSelect :: forall m. Html m Operation
opSelect = h "select" [] $ opOption <$>
  [Addition, Subtraction, Multiplication, Division]
 where
  opOption o = h "option"
    [ on "click" \_ -> o, "value" := opText o ]
    [ t $ opText o ]


num :: forall m. View Html m Int
num x = h "input"
  [ "value" := show x
  , onInput $ const <<< fromMaybe 0 <<< fromString
  ] []


view :: forall m. Functor m => View Html m Model
view model = h "div" []
  [ C.liftRV _{ left      = _ } _.left num model
  , C.liftR  _{ operation = _ } _.operation opSelect
  , C.liftRV _{ right     = _ } _.right num model
  , t $ " = " <> show (opFunction model.operation model.left model.right)
  ]
