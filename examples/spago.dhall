{ name = "examples"
, dependencies =
  [ "aff", "effect", "fluorine", "psci-support" ]
, packages = ../packages.dhall
, sources = [ "src/**/*.purs" ]
}
