{ name = "fluorine"
, dependencies =
  [ "aff", "effect", "halogen-vdom", "psci-support", "unsafe-reference" ]
, packages = ../packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
