-- | Interface for working with `Continuation`s.
-- |
-- | This module is intended to be imported as qualified, e.g.:
-- | ```
-- | import Fluorine.Continuation as C
-- | ```
module Fluorine.Continuation
  ( Continuation
  , pure, done, impure, kliesli, causes, causedBy, merge, before, after
  , runContinuation, hoist, write
  , class Continuous, cmap
  , lift, liftMay, View, liftR, liftRV, Lens', liftL, liftLV
  , void, forget, left, right, maybe, comaybe, either
  ) where

import Prelude

import Control.Apply (lift2)
import Control.Applicative as A
import Data.Bifunctor (lmap, rmap)
import Data.Const (Const (Const))
import Data.Either (Either (..))
import Data.Functor.Invariant (class Invariant, imap)
import Data.Maybe (Maybe (..), fromMaybe)
import Data.Maybe as M
import Data.Profunctor.Star (Star (Star))
import Data.Profunctor.Strong (class Strong)
import Data.Tuple (fst, snd)
import Data.Tuple.Nested (type (/\), (/\))
import Effect.Class (class MonadEffect)
import Effect.URef (URef)
import Effect.URef as URef
import Safe.Coerce (coerce)

-- | `Continuation m a` represents a builder of an atomic state update, which
-- | can be effectfully run in `m` using current state `a` to construct state
-- | update function `a -> a`. This allows asynchronously run continuations to
-- | perform effects and access state without interfering with changes of each
-- | other (unless some continuation explicitly decides to merge current
-- | changes into state while running using `merge`.)
-- |
-- | See functions below for more information about how to use it.
data Continuation m a
  = Pure (a -> a)
  | Rollback (Continuation m a)
  | Merge (Continuation m a)
  | Continuation (a -> a) (a -> m (Continuation m a))

instance invariantContinuaion :: Functor m => Invariant (Continuation m) where
  imap to from = case _ of
    Pure f           -> Pure $ to <<< f <<< from
    Rollback c       -> Rollback $ imap to from c
    Merge c          -> Merge $ imap to from c
    Continuation f c -> Continuation (to <<< f <<< from) $
      map (imap to from) <<< c <<< from

-- | `Semigroup` instance that combines continuations by interleaving their
-- | parallel stages, starting with stage on the right side (similarly to
-- | `(<<<)`).
instance semigroupContinuation :: Applicative m => Semigroup (Continuation m a)
 where
  append = case _, _ of
    Continuation f mc, Continuation g md ->
      Continuation (f <<< g) $ lift2 (lift2 append) mc md
    Continuation f mc, Rollback d        ->
      Rollback $ Continuation f $ map (_ <> d) <<< mc
    Rollback c       , Continuation _ md ->
      Rollback $ Continuation identity $ map (c <> _) <<< md
    Rollback c       , Rollback d        ->
      Rollback $ c <> d
    Pure f           , Pure g            ->
      Pure $ f <<< g
    Pure f           , Continuation g md ->
      Continuation (f <<< g) md
    Continuation f mc, Pure g            ->
      Continuation (f <<< g) mc
    Pure f           , Rollback d        ->
      Continuation f \_ -> A.pure $ Rollback d
    Rollback f       , Pure _            ->
      Rollback f
    Merge f          , g                 ->
      Merge $ f <> g
    f                , Merge g           ->
      Merge $ f <> g

-- | `mempty = done`
instance monoidContinuation :: Applicative m => Monoid (Continuation m a) where
  mempty = done

-- | Creates pure state update continuation.
pure :: forall m a. (a -> a) -> Continuation m a
pure = Pure

-- | Continuation that doesn't do anything with state.
done :: forall m a. Continuation m a
done = Pure identity

-- | Creates impure state update continuation.
impure :: forall m a. Functor m => m (a -> a) -> Continuation m a
impure = Continuation identity <<< const <<< map Pure

-- | Creates impure state update continuation, effects of which depend on
-- | current state.
kliesli :: forall m a. (a -> m (Continuation m a)) -> Continuation m a
kliesli = Continuation identity

-- | Creates continuation that performs effects without affecting the state.
causes :: forall m a x. Discard x => Functor m => m x -> Continuation m a
causes = impure <<< (<$) identity

-- | Creates continuation from effectful action that produces one.
causedBy :: forall m a. m (Continuation m a) -> Continuation m a
causedBy = Continuation identity <<< const

-- | Forces preceding state changes of continuation it appears in to be applied
-- | globally before continuing with it's argument.
merge :: forall m a. Continuation m a -> Continuation m a
merge = Merge

-- | Creates continuation that performs the first argument before the latter
-- | one.
before
  :: forall m a
   . Applicative m => Continuation m a -> Continuation m a -> Continuation m a
before = case _, _ of
  Pure f           , Continuation g md -> Continuation (g <<< f) md
  Pure _           , Rollback d        -> d
  Pure f           , Merge d           -> Continuation f \_ -> A.pure $ Merge d
  Pure f           , Pure g            -> Pure $ g <<< f
  Merge c          , d                 -> Merge $ before c d
  Rollback c       , d                 -> Rollback $ before c d
  Continuation f mc, d                 ->
    Continuation f $ map (_ `before` d) <<< mc

-- | Creates continuation that performs the first argument after the latter
-- | one.
after
  :: forall m a
   . Applicative m => Continuation m a -> Continuation m a -> Continuation m a
after = flip before

-- | Interprets `Continuation m a` into impure state update, using argument of
-- | type `a` as initial state.
-- |
-- | Because it works on `a` instead of `URef a`, the continuation can't
-- | interact with global state in any way throughout it's execution, and thus
-- | `merge` has no effect. For normal use cases, it's recommended to use
-- | `write` instead.
runContinuation :: forall m a. Monad m => Continuation m a -> a -> m (a -> a)
runContinuation = go identity where
  go :: (a -> a) -> Continuation m a -> a -> m (a -> a)
  go prev c s = case c of
    Pure f             -> A.pure $ f <<< prev
    Rollback c'        -> go identity c' s
    Merge c'           -> go prev c' s
    Continuation f mc' -> do
      c' <- mc' $ prev s
      go (f <<< prev) c' s

-- | Applies given transformation to effectful context `m` used in
-- | `Continuation m a`.
hoist :: forall m n. Functor m => (m ~> n) -> Continuation m ~> Continuation n
hoist nt = case _ of
  Pure f            -> Pure f
  Rollback c        -> Rollback $ hoist nt c
  Merge c           -> Merge $ hoist nt c
  Continuation f mc -> Continuation f $ nt <<< map (hoist nt) <<< mc

-- | Class of types which can be transformed using continuation-transforming
-- | functions. `Continuation` is trivially `Continuous`, other examples are
-- | `Html` or `Prop`, which carry state updates used in response to events.
-- |
-- | Formally, `f` in `Continuous f` is a functor from category of Purescript
-- | types and functions into category of `Continuation`s and functions.
class Continuous f where
  cmap
    :: forall m a b. (Continuation m a -> Continuation m b) -> f m a -> f m b

-- | `cmap = identity`.
instance continuousContinuation :: Continuous Continuation where
  cmap = identity

-- | Lifts `Continuous` type `c m a` (e.g. `Continuation m a` or `Html m a`)
-- | from `a` to `b`, using function that "inserts" value `a` into `b` and
-- | function that retrieves it.
lift
  :: forall c m a b
   . Continuous c
  => Functor m
  => (a -> b -> b) -> (b -> a) -> c m a -> c m b
lift set get = cmap go where
  go = case _ of
    Pure f            -> Pure $ modify f
    Rollback c        -> Rollback $ go c
    Merge c           -> Merge $ go c
    Continuation f mc -> Continuation (modify f) $ map go <<< mc <<< get
  modify f s = set (f $ get s) s

-- | As `lift`, but doesn't affect value of `b` in any way when retrieval
-- | function returns `Nothing`.
liftMay
  :: forall c m a b
   . Continuous c
  => Applicative m
  => (a -> b -> b) -> (b -> Maybe a) -> c m a -> c m b
liftMay set get = cmap go where
  go = case _ of
    Pure f            -> Pure $ modifyMay f
    Rollback c        -> Rollback $ go c
    Merge c           -> Merge $ go c
    Continuation f mc -> Continuation (modifyMay f) $
      M.maybe (A.pure done) (map go <<< mc) <<< get
  modifyMay f s = M.maybe identity (set <<< f) (get s) s

type View :: ((Type -> Type) -> Type -> Type) -> (Type -> Type) -> Type -> Type
-- | `View c m a` is a synonym for `a -> c m a`. It commonly appears in cases
-- | like `View Html m s`, where children element both depends on and updates
-- | some state `s`, that is, provides "view" of that state. It can be useful
-- | to avoid duplication of lenghty state types.
type View c m a = a -> c m a

-- | Version of `lift` with arguments of inserting function reversed. Useful
-- | in combination with wildcard record literals:
-- | ```
-- | liftR _{ field = _ } _.field ?element
-- | ```
liftR
  :: forall c m a b
   . Continuous c
  => Functor m
  => (b -> a -> b) -> (b -> a) -> c m a -> c m b
liftR = lift <<< flip

-- | Version of `liftR` operating on `View`s. Allows to shorten
-- | ```
-- | liftR _{ field = _ } _.field $ ?view ?model.field
-- | ```
-- | into
-- | ```
-- | liftRV _{ field = _ } _.field ?view ?model
-- | ```
liftRV
  :: forall c m a b
   . Continuous c
  => Functor m
  => (b -> a -> b) -> (b -> a) -> View c m a -> View c m b
liftRV set get view = lift (flip set) get <<< view <<< get

-- | Same as `Lens'` in `profunctor-lenses` package, provided separately to
-- | avoid unnecessary dependency.
type Lens' s a = forall p. Strong p => p a a -> p s s

lensSet :: forall a b. ((a -> a) -> b -> b) -> a -> b -> b
lensSet = (>>>) const

lensGet :: forall a b. (Star (Const a) a a -> Star (Const a) b b) -> b -> a
lensGet = coerce <<< (#) (Star Const)

-- | As `lift`, but uses `Lens' b a` instead of explicit functions.
liftL
  :: forall c m a b. Continuous c => Functor m => Lens' b a -> c m a -> c m b
liftL lens = lift (lensSet lens) (lensGet lens)

-- | Version of `liftL` operating on `View`s for convenience. Allows to shorten
-- | ```
-- | liftL ?lens $ ?view $ ?model ^. ?lens
-- | ```
-- | into
-- | ```
-- | liftLV ?lens ?view ?model
-- | ```
liftLV
  :: forall c m a b
   . Continuous c => Functor m => Lens' b a -> View c m a -> View c m b
liftLV lens view = liftL lens <<< view <<< lensGet lens

-- | Lifts `Continuous` type `c m Unit` (e.g. `Continuation m Unit` or
-- | `Html m Unit`) into one that works for any `a` instead of `Unit`. Useful
-- | e.g. with `Html` elements which don't produce state updates.
void :: forall c m a. Continuous c => Monad m => c m Unit -> c m a
void = cmap $
  Continuation identity <<< const <<< (<$) done <<< flip runContinuation unit

-- | "Forgets" the `a` in `c m a`. Useful e.g. for ignoring event handlers of
-- | `Html` element.
forget :: forall c m a b. Continuous c => c m a -> c m b
forget = cmap \_ -> done

-- | As `lift`, but specifically lifts `c m a` to work on the first element of
-- | pair `a /\ b`.
left :: forall c m a b. Continuous c => Functor m => c m a -> c m (a /\ b)
left = cmap $ lift (\a (_ /\ b) -> a /\ b) fst

-- | As `lift`, but specifically lifts `c m b` to work on the second element of
-- | pair `a /\ b`.
right :: forall c m a b. Continuous c => Functor m => c m b -> c m (a /\ b)
right = cmap $ lift (\b (a /\ _) -> a /\ b) snd

-- | Lifts `Continuous` type `c m a` (e.g. `Continuation m a` or `Html m a`)
-- | to work on `Maybe a`. In case of state being `Nothing`, no update happens.
maybe :: forall c m a. Continuous c => Applicative m => c m a -> c m (Maybe a)
maybe = cmap go where
  go = case _ of
    Pure f            -> Pure $ map f
    Rollback c        -> Rollback $ go c
    Merge c           -> Merge $ go c
    Continuation f mc -> Continuation (map f) $
      M.maybe (A.pure $ Rollback done) (map maybe <<< mc)

-- | Lifts `Continuous` type `c m (Maybe a)` (e.g. `Continuation m (Maybe a)`
-- | or `Html m (Maybe a)`) to work on `a`. In case of update being `Nothing`,
-- | no change happens.
comaybe :: forall c m a. Continuous c => Applicative m => c m (Maybe a) -> c m a
comaybe = cmap go where
  go = case _ of
    Pure f            -> Pure $ maybeApply f
    Rollback c        -> Rollback $ comaybe c
    Merge c           -> Merge $ comaybe c
    Continuation f mc -> Continuation (maybeApply f) $
      map comaybe <<< mc <<< Just
  maybeApply f x = fromMaybe x $ f $ Just x

-- | Creates `View` that operates on `Either a b` state, using left or right
-- | argument when the state correspondingly appears in `Left` or `Right`
-- | constructor. Can be used to implement e.g. simple single-page application
-- | layout, with arguments being views of sub-pages.
either
  :: forall c m a b
   . Continuous c
  => Monad m
  => View c m a -> View c m b -> View c m (Either a b)
either l r = case _ of
  Left  a -> cmap (flip go done) $ l a
  Right b -> cmap (     go done) $ r b
 where
  go c d = Continuation identity case _ of
    Left a -> case c of
      Pure f             -> A.pure $ Pure $ lmap f
      Rollback c'        -> A.pure $ Rollback $ go c' done
      Merge c'           -> A.pure $ Merge $ go c' done
      Continuation f mc' -> Continuation (lmap f) <<<
        const <<< A.pure <<< flip go (Rollback done) <$> mc' a
    Right b -> case d of
      Pure f             -> A.pure $ Pure $ rmap f
      Rollback d'        -> A.pure $ Rollback $ go done d'
      Merge d'           -> A.pure $ Merge $ go done d'
      Continuation f md' -> Continuation (rmap f) <<<
        const <<< A.pure <<< go (Rollback done) <$> md' b

-- | Executes supplied `Continuation m a`, applying resulting state update on
-- | given `URef a`.
-- |
-- | Keep in mind that Purescript with JS backend is single-threaded and
-- | expensive pure operations are blocking, without ability to run them in
-- | parallel way directly as in Haskell. Thus it's up to programmer to make
-- | sure that none of the actions in supplied continuation takes too much
-- | time, otherwise UI will block when calling `write` with it. Possible
-- | solutions are making use of asynchronous operations inside of `m` with
-- | `MonadAff`, and use of `delay` in expensive, otherwise pure operations to
-- | let event loop progress in reasonable intervals.
write :: forall m a. MonadEffect m => URef a -> Continuation m a -> m Unit
write ra = case _ of
  Pure f            -> modify_ f
  Rollback c        -> write ra c
  Merge c           -> write ra c
  Continuation f mc -> go f mc
 where
  go f mc  = URef.read ra >>= f >>> mc >>= case _ of
    Pure f'             -> modify_ $ f' <<< f
    Rollback c'         -> goOnly c'
    Merge c'            -> modify_ f *> goOnly c'
    Continuation f' mc' -> go (f' <<< f) mc'
  modify_  = flip URef.modify_ ra
  goOnly c = go identity \_ -> A.pure c
