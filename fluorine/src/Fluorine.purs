module Fluorine
  ( fluorine
  , Html, Prop, AttributeName, ClassName, ElementName, EventName
  , module Fluorine.Continuation
  , on, onM, onM_, onC, onRaw
  , onInput, onInputC
  , hoistHtml, hoistProp
  , h, t, attr, (:=), c
  ) where

import Prelude

import Control.Monad.Error.Class (throwError)
import Data.Bifunctor (lmap)
import Data.Either (Either (..))
import Data.Foldable (intercalate)
import Data.Functor.Invariant (class Invariant, imap)
import Data.Maybe (Maybe (..), maybe)
import Data.Newtype (wrap)
import Effect (Effect)
import Effect.Aff (Aff, launchAff_, makeAff, effectCanceler, nonCanceler)
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Exception (error)
import Effect.Ref as Ref
import Effect.Uncurried as E
import Effect.URef (URef)
import Effect.URef as URef
import Fluorine.Continuation (Continuation, class Continuous, View)
import Fluorine.Continuation as C
import Halogen.VDom (VDom, VDomSpec)
import Halogen.VDom as V
import Halogen.VDom.DOM.Prop as VP
import Safe.Coerce (coerce)
import Unsafe.Coerce (unsafeCoerce)
import Web.DOM (Document, Element)
import Web.DOM.Element (toNode) as D
import Web.DOM.Node (appendChild) as D
import Web.DOM.ParentNode (QuerySelector)
import Web.DOM.ParentNode (querySelector) as D
import Web.Event.Event (Event)
import Web.Event.Event (target) as D
import Web.Event.EventTarget (addEventListener, eventListener, removeEventListener) as D
import Web.HTML (window) as D
import Web.HTML.Event.EventTypes (domcontentloaded) as D
import Web.HTML.HTMLDocument (HTMLDocument)
import Web.HTML.HTMLDocument (toDocument, toParentNode, readyState) as D
import Web.HTML.HTMLDocument.ReadyState (ReadyState (..)) as D
import Web.HTML.Window (document, toEventTarget) as D

type RawHandler m a = Effect (Continuation m a)

newtype Html m a = Html (VDom (Array (VP.Prop (RawHandler m a))) Void)

type HtmlView m a = a -> Html m a

unHtml :: forall m a. Html m a -> VDom (Array (VP.Prop (RawHandler m a))) Void
unHtml (Html h') = h'

instance invariantHtml :: Functor m => Invariant (Html m) where
  imap to from (Html h') = Html $ lmap (map $ map $ map $ imap to from) h'

instance invariantProp :: Functor m => Invariant (Prop m) where
  imap to from (Prop p) = Prop $ map (map $ imap to from) p

newtype Prop m a = Prop (VP.Prop (RawHandler m a))

type AttributeName = String
type ClassName     = String
type ElementName   = String
type EventName     = String

on :: forall m a. EventName -> (a -> a) -> Prop m a
on e = onC e <<< C.pure

onM :: forall m a. Functor m => EventName -> m (a -> a) -> Prop m a
onM e = onC e <<< C.impure

onM_ :: forall m a. Functor m => EventName -> m Unit -> Prop m a
onM_ e = onC e <<< C.causes

onC :: forall m a. EventName -> Continuation m a -> Prop m a
onC e = onRaw e <<< const <<< Just <<< pure

onRaw :: forall m a. EventName -> (Event -> Maybe (RawHandler m a)) -> Prop m a
onRaw e = Prop <<< VP.Handler (wrap e)

onInput :: forall m a. (String -> a -> a) -> Prop m a
onInput = onInputC <<< map C.pure

onInputC :: forall m a. (String -> Continuation m a) -> Prop m a
onInputC f = onRaw "input" \e -> Just $ maybe
  (throwError $ error "onInputC: missing target")
  (unsafeCoerce >>> _.value >>> f >>> pure)
  (D.target e)

hoistHtml :: forall m n a. Functor m => (m ~> n) -> Html m a -> Html n a
hoistHtml nt (Html h') = Html $ lmap (map $ map $ map $ C.hoist nt) h'

hoistProp :: forall m n a. Functor m => (m ~> n) -> Prop m a -> Prop n a
hoistProp nt (Prop p) = Prop $ map (map $ C.hoist nt) p

h :: forall m a. ElementName -> Array (Prop m a) -> Array (Html m a) -> Html m a
h n ps cs = Html $ V.Elem Nothing (coerce n) (coerce ps) (coerce cs)

t :: forall m a. String -> Html m a
t = Html <<< V.Text

attr :: forall m a. AttributeName -> String -> Prop m a
attr a = Prop <<< VP.Property a <<< VP.propFromString

infix 0 attr as :=

instance continuousHtml :: Continuous Html where
  cmap f (Html h') = Html $ lmap (map $ map $ map f) h'

instance continuousProp :: Continuous Prop where
  cmap f (Prop p) = Prop $ map f <$> p

c :: forall m a. Array ClassName -> Prop m a
c = attr "class" <<< intercalate " "

fluorine
  :: forall m a
   . MonadEffect m
  => (a -> a -> Boolean)
  -> (m ~> Aff)
  -> a
  -> (a -> Html m a)
  -> Aff Unit
fluorine eq toAff a view = do
  r       <- URef.new eq a
  body    <- awaitBody
  liftEffect do
    doc     <- windowDocument
    let spec = fluorineSpec toAff r $ D.toDocument doc
    init    <- E.runEffectFn1 (V.buildVDom spec) $ unHtml $ view a
    machine <- Ref.new init
    _ <- D.appendChild (V.extract init) $ D.toNode body
    _ <- URef.subscribe r \a' -> do
      m <- Ref.read machine
      flip Ref.write machine =<< E.runEffectFn2 V.step m (unHtml $ view a')
    pure unit

fluorineSpec
  :: forall m a
   . MonadEffect m
  => (m ~> Aff)
  -> URef a -> Document -> VDomSpec (Array (VP.Prop (RawHandler m a))) Void
fluorineSpec toAff r document = V.VDomSpec
  { buildWidget: \_ -> E.mkEffectFn1 absurd
  , buildAttributes: VP.buildProp $ launchAff_ <<< toAff <=< map (C.write r)
  , document
  }

windowDocument :: Effect HTMLDocument
windowDocument = D.window >>= D.document

awaitLoad :: Aff Unit
awaitLoad = makeAff \k -> windowDocument >>= D.readyState >>= case _ of
  D.Loading -> do
    et <- D.toEventTarget <$> D.window
    el <- D.eventListener \_ -> k $ Right unit
    D.addEventListener D.domcontentloaded el false et
    pure $ effectCanceler $
      D.removeEventListener D.domcontentloaded el false et
  _ -> nonCanceler <$ (k $ Right unit)

awaitBody :: Aff Element
awaitBody = do
  awaitLoad
  selectElement (wrap "body") >>= flip maybe pure do
    throwError $ error "fluorine: Could not find body"

selectElement :: QuerySelector -> Aff (Maybe Element)
selectElement query = liftEffect $
  D.querySelector query <<< D.toParentNode =<< windowDocument
