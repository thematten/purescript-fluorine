-- | Interface for working with `URef`s.
-- |
-- | This module is intended to be imported as qualified, e.g.:
-- | ```
-- | import Effect.URef (URef)
-- | import Effect.URef as URef
-- | ```
module Effect.URef
  ( URef, Subscription
  , new, subscribe, unsubscribe, read, write, modify, modify_
  ) where

import Prelude

import Data.Array (deleteBy)
import Data.Traversable (traverse_)
import Effect (Effect)
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Ref (Ref)
import Effect.Ref as Ref
import Unsafe.Reference (unsafeRefEq)

-- | `URef a` is a mutable reference to value of type `a`, with ability to
-- | subscribe to value changes. You can create a new one using `new`.
newtype URef a = URef
  { ref  :: Ref a
  , eq   :: a -> a -> Boolean
  , subs :: Ref (Array (a -> Effect Unit))
  }

-- | `Subscription` is a handle to subscription created using `subscribe`. You
-- | can use it in combination with `unsubscribe` to remove such subscription.
newtype Subscription = Subscription (Effect Unit)

-- | Creates new `URef a` using initial value and function that compares old
-- | and new value for equality.
new :: forall m a. MonadEffect m => (a -> a -> Boolean) -> a -> m (URef a)
new eq a = URef <$> liftEffect do
  { ref: _, eq, subs: _ } <$> Ref.new a <*> Ref.new []

-- | Creates new subscription to `URef a`, using function that gets called with
-- | a new value of `a` on every change of it's content.
subscribe
  :: forall m a. MonadEffect m => URef a -> (a -> Effect Unit) -> m Subscription
subscribe (URef r) f = liftEffect do
  Ref.modify_ (_ <> [f]) r.subs
  pure $ Subscription $ Ref.modify_ (deleteBy unsafeRefEq f) r.subs

-- | Unsubscribes given `Subscription` from it's reference.
unsubscribe :: forall m. MonadEffect m => Subscription -> m Unit
unsubscribe (Subscription f) = liftEffect f

-- | Reads current value `a` of `URef a`.
read :: forall m a. MonadEffect m => URef a -> m a
read (URef r) = liftEffect $ Ref.read r.ref

-- | Writes new value `a` into `URef a`, notifying subscribers if the new value
-- | is different according to reference's predicate.
write :: forall m a. MonadEffect m => a -> URef a -> m Unit
write a (URef r) = liftEffect $ Ref.read r.ref >>= flip r.eq a >>>
  if _ then pure unit else do
    Ref.write a r.ref
    traverse_ (_ $ a) =<< Ref.read r.subs

-- | Modifies value `a` of `URef a`, notifying subscribers if the new value is
-- | different according to reference's predicate.
modify_ :: forall m a. MonadEffect m => (a -> a) -> URef a -> m Unit
modify_ f (URef r) = liftEffect do
  flip write (URef r) <<< f =<< Ref.read r.ref

-- | Modifies value `a` of `URef a`, notifying subscribers if the new value is
-- | different according to reference's predicate, and returns new value.
modify :: forall m a. MonadEffect m => (a -> a) -> URef a -> m a
modify f (URef r) = liftEffect do
  a' <- f <$> Ref.read r.ref
  write a' (URef r) $> a'
